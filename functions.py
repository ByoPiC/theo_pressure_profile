####Call of the useful libraries ####
import illustris_python as il
import matplotlib.pyplot as plt
import matplotlib as mpl
import math
import numpy as np
from astropy import constants as const
from astropy import units as u
import time
import pandas as pd
from scipy.optimize import curve_fit
from  scipy.stats import chisquare
import csv
from scipy.integrate import quad
from scipy.stats import chisquare

#### Constants ####
h=0.6774
Xh=0.76 
mp=const.m_p.to_value('kg')/h
Kb=const.k_B.to_value('J/K')
gamma=5/3
#rho_c=cosmo.critical_density(0)

##### Functions ##### 
#1: "Deep" functions: opend, mean_r, sub500, comp500, r500m500computing, pressstorage
#2:  Big functions: massbin,clszhdeki,massbin,massbin0,massbinTNG300
#3:  Maths functions (UPP,UPPlog,...)
#4:  Little plotting functions used for tests
##N.B.: Many functions are in double, one for the FoF catalog and the other for the Subfind catalog


###"Deep functions"####

def opend(sim,snap,hid,hos):#Open the dataset of an halo and its group_catalog dataset, sim is for the name of the simulation (TNG100_3 for example), snap is for the number of the snapshot (99 for z=0), hid is for the id of the halo (from 0 to ... in the catalogs), hos is for choosing if we go into friends of friends (0) or subfind (1) catalogs
    p='/data/cluster/byopic/SIMS/'+sim #Gives the path to the data in the server
    if(hos==0):#Fof
        d=il.snapshot.loadHalo(p,snap,hid,'gas')
        g=il.groupcat.loadSingle(p,snap,haloID=hid)
        halocenter=g['GroupCM']
        haloR500=g['Group_R_Crit500']
        m500=g['Group_M_Crit500']
        
    if(hos==1):#Biggest subhalo of the halo (Subfinds) (because in the subfinds catalogs there's no r500 and m500 we load the file where they've been stored after computation, the function doing it is further in the code)
        g=il.groupcat.loadSingle(p,snap,haloID=hid)
        firstsub=g['GroupFirstSub']
        d=il.snapshot.loadSubhalo(p,snap,firstsub,'gas')
        s=il.groupcat.loadSingle(p,snap,subhaloID=firstsub)
        halocenter=s['SubhaloCM']
        if(sim=="TNG100_3"):
            a=pd.read_csv("r500-m500_0-1000Subfind.csv") #file containing m500 and r500 values for the halo, the file is in the directory /data/cluster/tlebeau
            haloR500=a["r500"][hid]
            m500=a["m500"][hid]
        elif(sim=="TNG300_1"):
            a=pd.read_csv("rc_mc-TNG300-1_0-2000.csv") #same
            haloR500=a["rc"][hid]
            m500=a["mc"][hid]
            
    return d,g,halocenter,haloR500,m500 ##returns the dataset(d), the halo general informations from the catalog (g), the center of mass of the halo, R500 and m500


def mean_r(d,halocenter): #calculation of the  mean pressure in fonction of radial distance to the center (d is the name of the dataset), the bin is 1kpc
    dist=((halocenter[0]-d['Coordinates'][:,0])**2+(halocenter[1]-d['Coordinates'][:,1])**2+(halocenter[2]-d['Coordinates'][:,2])**2)**0.5 #calculating the distance for each particle
    mi=min(dist)
    ma=max(dist)
    ##calulating the electron density and temperature using the equations from the TNG website (see FAQ)
    ne=(d['ElectronAbundance']*Xh*d['Density']*0.6774)/(const.m_p.to_value('kg'))
    n=len(dist)
    mu=(4*const.m_p.to_value('kg')/(1+3*Xh+4*Xh*d['ElectronAbundance']))
    T=((gamma-1)*d['InternalEnergy']*mu*10E12)/Kb
    ##creating the arrays and putting the particles in the good distance cell and doing the mean (density weighted by the volume) 
    temp=np.zeros(6000)
    den=np.zeros(6000)
    nb=np.zeros(6000)
    nbv=np.zeros(6000)
    vol=d['Masses']/d['Density']
    
    for i in range(0,n):
        if(int(dist[i])<6000):
            den[int(dist[i])]+=ne[i]*vol[i]
            temp[int(dist[i])]+=T[i]
            nbv[int(dist[i])]+=vol[i]
            nb[int(dist[i])]+=1
        
    for j in range(0,6000):
        if(den[j]>0):
            den[j]/=nbv[j]
        if(temp[j]>0):
            temp[j]/=nbv[j]
    P=den*temp*Kb
    for i in range(0,6000):#putting nans instead of zeros in order to correct mean and std problems
        if(den[i]==0):
            den[i]=np.nan
        if(temp[i]==0):
            temp[i]=np.nan
        if(nb[i]==0):
            nb[i]=np.nan
        if(P[i]==0):
            P[i]=np.nan
    return mi,ma,den,temp,P,nb #returns the minimum and maximum distance to the center, the density,temperature and pressure arrays, and the number of particles at each bin (used after)


def sub500(h):#computing of r500 and m500 for the biggest subhalo of the halo of h id
    sim='TNG300_1'
    m_dm1003=3.2356E-2
    m_dm3001=3.9834E-3
    p='/data/cluster/byopic/SIMS/'+sim
    g=il.groupcat.loadSingle(p,99,haloID=h)
    firstsub=g['GroupFirstSub']
    s=il.groupcat.loadSingle(p,99,subhaloID=firstsub)
    haloR500=g['Group_R_Crit500']
    halocenter=g['GroupPos']
    m500=g['Group_M_Crit500']
    rho500=(3*m500)/(4*np.pi*(haloR500**3))
    gas=darkmatter=stars=bh=0
    print("r500sim= "+str(haloR500))
    print("rho500= "+str(rho500))
    
    d=il.snapshot.loadSubhalo(p,99,firstsub,'gas',fields=['Coordinates','Masses'])
    if(len(d)<2):
        print('no gas particles')
    else:
        dist=((halocenter[0]-d['Coordinates'][:,0])**2+(halocenter[1]-d['Coordinates'][:,1])**2+(halocenter[2]-d['Coordinates'][:,2])**2)**0.5
        gas=1
        
    dm=il.snapshot.loadSubhalo(p,99,firstsub,'dm',fields=['Coordinates'])
    if(len(dm)<2):
        print('no dm particles')
    else:
        distm=((halocenter[0]-dm[:,0])**2+(halocenter[1]-dm[:,1])**2+(halocenter[2]-dm[:,2])**2)**0.5
        darkmatter=1
        
    ds=il.snapshot.loadSubhalo(p,99,firstsub,'stars',fields=['Coordinates','Masses'])
    if(len(ds)<2):
        print('no stars particles')
    else:
        dists=((halocenter[0]-ds['Coordinates'][:,0])**2+(halocenter[1]-ds['Coordinates'][:,1])**2+(halocenter[2]-ds['Coordinates'][:,2])**2)**0.5
        stars=1
        
    dbh=il.snapshot.loadSubhalo(p,99,firstsub,'bh',fields=['Coordinates','Masses'])
    if(len(dbh)<2):
        print('no bh particles')
    else:
        distbh=((halocenter[0]-dbh['Coordinates'][:,0])**2+(halocenter[1]-dbh['Coordinates'][:,1])**2+(halocenter[2]-dbh['Coordinates'][:,2])**2)**0.5
        bh=1
    rho=1
    j=0
    while(rho>rho500):
        j+=1
        m=[]
        mdm=[]
        ms=[]
        mbh=[]
        v=mt=mdmt=mst=mbht=0
        mtot=0
        if(gas==1):
            m=[d['Masses'][i] for i in range(len(dist)) if dist[i]<j]
            mt=np.sum(m)
        if(darkmatter==1):
            mdm=[1 for i in range(len(distm)) if distm[i]<j]
            mdmt=np.sum(mdm)     
            mdmt*=m_dm3001
        if(stars==1):
            ms=[ds['Masses'][i] for i in range(len(dists)) if dists[i]<j]
            mst=np.sum(ms)
        if(bh==1):
            mbh=[dbh['Masses'][i] for i in range(len(distbh)) if distbh[i]<j]
            mbht=np.sum(mbh)
        mtot=mt+mdmt+mst+mbht
        v=(4*np.pi*j**3)/3
        if(v!=0):
            rho=mtot/v
        else:
            rho=1
        print("haloId: "+str(h))  
        print(j)
        print(rho)
     
    print("haloR500= "+str(haloR500))
    print("m500= "+str(m500))
    return j,mtot

def comp500(h):#recomputing of m500 and r500 for halos from friends-of-friends to check if the method is good, same comments as the previous function 
    sim='TNG300_1'
    m_dm1003=3.2356E-2
    m_dm3001=3.9834E-3
    p='/data/cluster/byopic/SIMS/'+sim
    g=il.groupcat.loadSingle(p,99,haloID=h)
    haloR500=g['Group_R_Crit500']
    halocenter=g['GroupPos']
    m500=g['Group_M_Crit500']
    rho500=(3*m500)/(4*np.pi*(haloR500**3))
    print('R500sim= '+str(haloR500))
    print('rho500= '+str(rho500))
    gas=darkmatter=stars=bh=0
    d=il.snapshot.loadHalo(p,99,h,'gas',fields=['Coordinates','Masses'])
    
    if(len(d)<2):
        print('no gas particles')
    else:
        dist=((halocenter[0]-d['Coordinates'][:,0])**2+(halocenter[1]-d['Coordinates'][:,1])**2+(halocenter[2]-d['Coordinates'][:,2])**2)**0.5
        gas=1
        
    dm=il.snapshot.loadHalo(p,99,h,'dm',fields=['Coordinates'])
    if(len(dm)<2):
        print('no dm particles')
    else:
        distm=((halocenter[0]-dm[:,0])**2+(halocenter[1]-dm[:,1])**2+(halocenter[2]-dm[:,2])**2)**0.5
        darkmatter=1
        
    
    ds=il.snapshot.loadHalo(p,99,h,'stars',fields=['Coordinates','Masses'])
    if(len(ds)<2):
        print('no stars particles')
    else:
        dists=((halocenter[0]-ds['Coordinates'][:,0])**2+(halocenter[1]-ds['Coordinates'][:,1])**2+(halocenter[2]-ds['Coordinates'][:,2])**2)**0.5
        stars=1
        
    dbh=il.snapshot.loadHalo(p,99,h,'bh',fields=['Coordinates','Masses'])
    if(len(dbh)<2):
        print('no bh particles')
    else:
        distbh=((halocenter[0]-dbh['Coordinates'][:,0])**2+(halocenter[1]-dbh['Coordinates'][:,1])**2+(halocenter[2]-dbh['Coordinates'][:,2])**2)**0.5
        bh=1
    rho=1
    j=0
    mtot=0
    while(rho>rho500):
        j+=1
        m=[]
        mdm=[]
        ms=[]
        mbh=[]
        v=mt=mdmt=mst=mbht=mtot=0
        if(gas==1):
            m=[d['Masses'][i] for i in range(len(dist)) if dist[i]<j]
            mt=np.sum(m)
        if(darkmatter==1):
            mdm=[1 for i in range(len(distm)) if distm[i]<j]
            mdmt=np.sum(mdm)     
            mdmt*=m_dm3001
        if(stars==1):
            ms=[ds['Masses'][i] for i in range(len(dists)) if dists[i]<j]
            mst=np.sum(ms)
        if(bh==1):
            mbh=[dbh['Masses'][i] for i in range(len(distbh)) if distbh[i]<j]
            mbht=np.sum(mbh)
        mtot=mt+mdmt+mst+mbht
        v=(4*np.pi*j**3)/3
        if(v!=0):
            rho=mtot/v
        else:
            rho=1
        print("haloId: "+str(h))    
        print(j)
        print(rho)
    return j,haloR500,mtot,m500


def r500m500computing():#To save the calculated r500 and m500 for clusters of ID in some range 
    data=[]
    for i in range(1027,1200): #ange of clusters for wich we do the calculation, you can change from sub500 to comp500
        print('halo ID: '+str(i))
        rc,mc=sub500(i)
        print('comp r_500= '+str(rc)+' kpc')
        print('comp m_500= '+str(mc)+' 1E10M_sun')
        data.append([rc,mc])
        
    with open('rc_mc-TNG300-1_1027_1199.csv','w') as d: #saving the data in this file
        writer=csv.writer(d)
        writer.writerows(data)

def press_storage(sim,snap,deb,fin,hos): #Saving the normalized pressure profiles for the clusters of ID from deb to fin in an .npy file (instead of csv before) 
    press=[]
    for i in range(deb,fin):
        print(i)
        d,g,halocenter,haloR500,m500=opend(sim,snap,i,hos)
        mi,ma,n,t,P,nb=mean_r(d,halocenter)
        P_500=np.nansum(P[0:int(haloR500)+1]*nb[0:int(haloR500)+1])/np.nansum(nb[0:int(haloR500)+1])
        P/=P_500
        press.append(P)
        
    with open('PdivP500_TNG300-1_0-1000_Sub.npy','wb') as h:
        np.save(h,press)


        
####Big functions###
            

def massbin(sim,snap,deb,fin,hos):#classification of the clusters by mass in bins of .2 (or other) in log10 scale , hos=0 for Fof, hos=1 for Subfinds in TNG100_3 sim, press log bin, fitting and plotting 
    mbin=[]
    pressure=[]
    pressurefit=[]
    r500=[]
    for i in range(deb,fin): #selection of the clusters of ID from deb to fin 
        print(i)
        d,g,halocenter,haloR500,m500=opend(sim,snap,i,hos)
        #print(m500)
        mbin.append(np.log10(m500*1E10)) #put mass in log scale
        mi,ma,n,t,P,nb=mean_r(d,halocenter)
        P_500=np.nansum(P[0:int(haloR500)+1]*nb[0:int(haloR500)+1])/np.nansum(nb[0:int(haloR500)+1])
        P/=P_500
        print("P= "+str(P))
        plog=np.zeros(len(P))
        for i in range(0,len(P)):
            if(P[i]>0):
                plog[i]=np.log10(P[i])
            else:
                plog[i]=np.nan
                
        print("plog= "+str(plog))
        pressure.append(plog)
        r500.append(haloR500)
        
    #2nd part of the function for pressure binning, fitting and subplots (previously meanprofilebin)
    binmeanpress=[]
    fitparam=[]
    fitcov=[]
    plotnum=0
    plt.figure(figsize=(12,12))
    
    for j in range(0,22,2): #Place to change the mass range of binning (linked to the mass binning change )
        std=np.zeros(6000)
        
        print(j)
        meanpress=np.zeros((100,6000))
        meanr=0
        r=range(0,6000)
        n=0
        nl=np.zeros(6000)

        
        ######### classification using the mass binning #####
        for i in range(0,len(mbin)):
            if(mbin[i]<15.2-j*0.1 and mbin[i]>15-j*0.1):  ###Place to change the mass binning 
                meanpress[n,:]=pressure[i][:]
                meanr+=r500[i]
                n+=1
        
        print('n= '+str(n))
        print(meanpress)
        if(n>1):
            for k in range(0,6000): ###standard deviation
                std[k]=np.nanstd(meanpress[0:n,k])
            print(std)
            plotnum+=1
            meanpresstot=np.zeros(6000)
            for l in range(0,6000):
                meanpresstot[l]=np.nanmean(meanpress[0:n,l])
            meanr/=n
            r/=meanr
            print("meanpresstot"+str(meanpresstot))
            #####pressure +std binning ######
            rbin=[np.log10(i) for i in range(1,len(meanpresstot))]
            Pbin=np.zeros(80)
            stdbin=np.zeros(80)
            for l in range(0,80):
                m=0
                for i in range(0,len(meanpresstot)-1):
                    if(rbin[i]<((l+1)*0.05) and rbin[i]>(l*0.05)):
                        Pbin[l]+=meanpresstot[i]
                        stdbin[l]+=std[i]
                        m+=1
                        
                if(m!=0):
                    Pbin[l]/=m
                    stdbin[l]/=m
            for h in range(0,len(Pbin)):
                if(Pbin[h]==0):
                    Pbin[h]=np.nan
                    stdbin[h]=np.nan
            
            rdebin=[10**(i*0.05) for i in range(80)]
            rdebin/=meanr
            
            ###fitting #####
            deb=20
            fin=60
            xfit=np.zeros(fin)
            yfit=np.zeros(fin)
            stdfit=np.zeros(fin)
    
            for i in range(0,fin):
                yfit[i]=-10
                xfit[i]=-10
                stdfit[i]=-10
                if(np.isnan(Pbin[i])==False):
                   yfit[i]=Pbin[i]
                   xfit[i]=rdebin[i]
                   stdfit[i]=stdbin[i]
                   
            for i in range(0,fin):
                if(stdfit[i]==0):
                    stdfit[i]=1E-3
                    
            for i in range(0,deb):
                yfit[i]=-10
                xfit[i]=-10
                stdfit[i]=-10
                
            print("Pbin= "+str(Pbin))
            print("yfit= "+str(yfit))
            popt,pcov=curve_fit(UPPlog,xfit[xfit>-10],yfit[yfit>-10],sigma=stdfit[stdfit>-10],p0=[6.41,1.81,1.33,4.13,0.31],bounds=([0,0,0,0,-10],[600,20,10,15,10]),method='trf')
            pcovdiag=[pcov[i][i] for i in range(0,5)]
            print("best fit params="+str(popt))

            #####Plotting #####
            min=round(15.1-j*0.1,2)
            title='fit: $P_{0}$='+str(round(popt[0],2))+', $c_{500}$='+str(round(popt[1],2))+', $\gamma$='+str(round(popt[4],2))+' ,'+chr(945)+'='+str(round(popt[2],2))+' ,'+chr(946)+'='+str(round(popt[3],2))
            plt.subplot(3,3,plotnum)
            plt.scatter(rdebin[deb:fin],Pbin[deb:fin],s=3)
            plt.errorbar(rdebin[deb:fin],Pbin[deb:fin],yerr=stdbin[deb:fin],marker='.',ls='None',label='TNG100_3_Sub')
            pUPP=[6.41,1.81,1.33,4.13,0.31]
            plt.plot(rdebin,UPPlog(rdebin,*pUPP),color='black',label='UPP')
            plt.xlim(0.01,10)
            plt.ylim(-4,3)
            plt.xscale('log')
            plt.xlabel("$R/R_{500}$")
            plt.ylabel("$P/P_{500}$")
            plt.title('$M_{500}$='+str(min)+'$\pm0.1$E10$M_{\odot}/h$'+',$n_{elem}$= '+str(n))
            plt.legend()
            fitparam.append(popt)
            fitcov.append(pcovdiag)

    ###Saving the fitting parameters
            
    #with open('fitparam14,1to12,9Sub2506log.csv','w') as d:
    #    writer=csv.writer(d)
    #    writer.writerows(fitparam)

    #with open('fitparam14,1to12,9Sub2506logcov.csv','w') as da:
    #    writer=csv.writer(da)
    #    writer.writerows(fitcov)
            
    print(fitparam)
    plt.tight_layout(pad=0.4)
    #plt.legend()
    #plt.show()


def massbinO(sim,snap,deb,fin,hos):#classification of the clusters by mass in bins of .2 (or other) in log10 scale , hos=0 for Fof, hos=1 for Subfinds for TNG100_3 sim, used for overplotting the Fof and Subfind profiles by the overplot function. 
    mbin=[]
    pressure=[]
    pressurefit=[]
    r500=[]
    for i in range(deb,fin):
        print(i)
        d,g,halocenter,haloR500,m500=opend(sim,snap,i,hos)
        mbin.append(np.log10(m500*1E10))
        mi,ma,n,t,P,nb=mean_r(d,halocenter)
        print(P)
        #print(nb)
        P_500=np.nansum(P[0:int(haloR500)+1]*nb[0:int(haloR500)+1])/np.nansum(nb[0:int(haloR500)+1])
        P/=P_500
        plog=np.zeros(len(P))
        for i in range(0,len(P)):
            if(P[i]>0):
                plog[i]=np.log10(P[i])
            else:
                plog[i]=np.nan
                
        print("plog= "+str(plog))
        pressure.append(plog)
        r500.append(haloR500)
    print(pressure)

        
    #2nd part of the function (previously meanprofilebin)
    
    binmeanpress=[]
    plotnum=0
    rdebintot=[]
    stdtot=[]
    Pbintot=[]
    mintot=[]
    ntot=[]
    
    for j in range(0,22,2):
        std=np.zeros(6000)
        print(j)
        meanpress=np.zeros((100,6000))
        meanr=0
        r=range(0,6000)
        n=0
        nl=np.zeros(6000)

        
        ######### classification using the mass binning #####
        for i in range(0,len(mbin)):
            if(mbin[i]<15.2-j*0.1 and mbin[i]>15-j*0.1):
                meanpress[n,:]=pressure[i][:]
                meanr+=r500[i]
                n+=1
                
        print('n= '+str(n))
        
        if(n>1):
            for k in range(0,6000): ###standard deviation
                std[k]=np.nanstd(meanpress[0:n,k])
            plotnum+=1
            meanpresstot=np.zeros(6000)
            for l in range(0,6000):
                meanpresstot[l]=np.nanmean(meanpress[0:n,l])
            meanr/=n
            r/=meanr
            
            #####pressure +std binning ######
            rbin=[np.log10(i) for i in range(1,len(meanpresstot))]
            Pbin=np.zeros(80)
            stdbin=np.zeros(80)
            for l in range(0,80):
                m=0
                for i in range(0,len(meanpresstot)-1):
                    if(rbin[i]<((l+1)*0.05) and rbin[i]>(l*0.05)):
                        Pbin[l]+=meanpresstot[i]
                        stdbin[l]+=std[i]
                        m+=1

                if(m!=0):
                    Pbin[l]/=m
                    stdbin[l]/=m
            for h in range(0,len(Pbin)):
                if(Pbin[h]==0):
                    Pbin[h]=np.nan
                    stdbin[h]=np.nan
            
            rdebin=[10**(i*0.05) for i in range(80)]
            #Pdebin=np.zeros(1001)
            rdebin/=meanr
            
            #####Data preparation for plotting #####
            min=round(15.1-j*0.1,2)
            rdebintot.append(rdebin)
            Pbintot.append(Pbin)
            stdtot.append(stdbin)
            mintot.append(min)
            ntot.append(n)
        print(mintot)
        print(Pbintot)
    return rdebintot,Pbintot,stdtot,mintot,ntot

def massbinTNG300(deb,fin,hos):#classification of the clusters by mass in bins of .2 (or other) in log10 scale , hos=0 for Fof, hos=1 for Subfinds for TNG300_1 sim, same comments as the previous function 
    mbin=[]
    pressure=[]
    pressurefit=[]
    press=[]
    r500=[]
    ### Open the saved pressure profiles and r500 and m500
    if(hos==0):
        with open('r500+m500TNG300-1_0-150_Fof.npy','rb') as f:
            r500m500=np.load(f)
        with open('PdivP500_TNG300-1_0-150_Fof.npy','rb') as h:
            P=np.load(h)
    if(hos==1):
        with open('PdivP500_TNG300-1_0-1000_Sub.npy','rb') as h:
            P=np.load(h)
        a=pd.read_csv("rc_mc-TNG300-1_0-2000.csv")
            
    
    
    for i in range(deb,fin):
        print(i)
        #print(m500)
        if(hos==0):
            mbin.append(np.log10(r500m500[i][1]*1E10))
            r500.append(r500m500[i][0])
        if(hos==1):
            mbin.append(np.log10(a["mc"][i]*1E10))
            r500.append(a["rc"][i])
            
        plog=np.zeros(len(P[i]))
        for j in range(0,len(P[i])):
            if(P[i][j]>0):
                plog[j]=np.log10(P[i][j])
            else:
                plog[j]=np.nan
                
        print("plog= "+str(plog))
    
        pressure.append(plog)
        
    #2nd part of the function for pressure binning, fitting and subplots (previously meanprofilebin)
    binmeanpress=[]
    fitparam=[]
    fitcov=[]
    plotnum=0
    #if(hos==0):
    plt.figure(figsize=(12,12))
    
    for j in range(0,24,4):
    #for j in range(0,1):
        std=np.zeros(6000)
        
        print(j)
        meanpress=np.zeros((1100,6000))
        meanr=0
        r=range(0,6000)
        n=0
        nl=np.zeros(6000)

        
        ######### classification using the mass binning #####
        for i in range(0,len(mbin)):
            if(mbin[i]<15.2-j*0.1 and mbin[i]>14.8-j*0.1):
            #if(mbin[i]<15.1 and mbin[i]>12.8):
                meanpress[n,:]=pressure[i][:]
                meanr+=r500[i]
                n+=1
                
        print('n= '+str(n))
        if(n>1):
            for k in range(0,6000): ###standard deviation
                std[k]=np.nanstd(meanpress[0:n,k])
                
            plotnum+=1
            meanpresstot=np.zeros(6000)
            for l in range(0,6000):
                meanpresstot[l]=np.nanmean(meanpress[0:n,l])
            meanr/=n
            r/=meanr
            print("meanpresstot"+str(meanpresstot))
            #####pressure +std binning ######
            rbin=[np.log10(i) for i in range(1,len(meanpresstot))]
            Pbin=np.zeros(80)
            stdbin=np.zeros(80)
            for l in range(0,80):
                m=0
                for i in range(0,len(meanpresstot)-1):
                    if(rbin[i]<((l+1)*0.05) and rbin[i]>(l*0.05)):
                        Pbin[l]+=meanpresstot[i]
                        stdbin[l]+=std[i]
                        m+=1

                if(m!=0):
                    Pbin[l]/=m
                    stdbin[l]/=m
            for h in range(0,len(Pbin)):
                if(Pbin[h]==0):
                    Pbin[h]=np.nan
                    stdbin[h]=np.nan
            
            rdebin=[10**(i*0.05) for i in range(80)]
            rdebin/=meanr
            ###fitting #####
            deb=0
            fin=60
            xfit=np.zeros(fin)
            yfit=np.zeros(fin)
            stdfit=np.zeros(fin)
            for i in range(0,fin):
                yfit[i]=-10
                xfit[i]=-10
                stdfit[i]=-10
                if(np.isnan(Pbin[i])==False):
                   yfit[i]=Pbin[i]
                   xfit[i]=rdebin[i]
                   stdfit[i]=stdbin[i]
            for i in range(0,fin):
                if(stdfit[i]==0):
                    stdfit[i]=1E-3
            print('stdfit: '+str(stdfit))
            #popt,pcov=curve_fit(UPPlog,rdebin[deb:fin],Pbin[deb:fin],sigma=stdbin[deb:fin],p0=[6.41,1.81,1.33,4.13,0.31],bounds=([0,0,0,0,-10],[600,20,10,15,10]),method='trf')
            #popt,pcov=curve_fit(UPPlog,xfit[xfit>-10],yfit[yfit>-10],sigma=stdfit[stdfit>-10],p0=[6.41,1.81,1.33,4.13,0.31],bounds=([0,0,0,0,-10],[600,20,10,15,10]),absolute_sigma=True,method='trf')
            popt,pcov=curve_fit(UPPlog,xfit[xfit>-10],yfit[yfit>-10],sigma=stdfit[stdfit>-10],p0=[6.41,1.81,1.6163,4.13,0.096],bounds=([0,0,1.6162,0,0.095],[600,20,1.6164,15,0.097]),absolute_sigma=True,method='trf')
            #popt,pcov=curve_fit(betamodel,xfit[xfit>-10],yfit[yfit>-10],sigma=stdfit[stdfit>-10],absolute_sigma=True)
            pcovdiag=[pcov[i][i] for i in range(len(popt))]
            print("best fit params="+str(popt))
            print("covariance matrix diag"+str(pcovdiag))
            #chiq=chisquare(yfit[yfit>-10],betamodel(xfit[xfit>-10],*popt))
            #dof=len(xfit[xfit>-10])-len(popt)
            #chi2=chiq[0]/dof
            #print(chiq[0])
            #print(dof)
            #print("reduced chi2"+str(chi2))
            #####Plotting #####
            min=round(15-j*0.1,2)
            #title='fit: $P_{0}$='+str(round(popt[0],2))+', $c_{500}$='+str(round(popt[1],2))+', $\gamma$='+str(round(popt[4],2))+' ,'+chr(945)+'='+str(round(popt[2],2))+' ,'+chr(946)+'='+str(round(popt[3],2))
            if(hos==0):
                titre='TNG300_1 Fof, $n_{elem}$='+str(n)
            else:
                titre='TNG300_1 Sub'
            plt.subplot(3,2,plotnum)
            plt.scatter(rdebin[deb:fin],Pbin[deb:fin],s=3)
            plt.errorbar(rdebin[deb:fin],Pbin[deb:fin],yerr=stdbin[deb:fin],marker='.',ls='None',label=titre)
            plt.plot(rdebin,UPPlog(rdebin,*popt),label=' gNFW fit, $P_0$='+str(np.round(popt[0],3))+', $c_{500}$='+str(np.round(popt[1],3))+', $\gamma$='+str(np.round(popt[4],3))+', '+chr(945)+'='+str(np.round(popt[2],3))+', '+chr(946)+'='+str(np.round(popt[3],3)))
#$\chi^{2}$='+str(np.round(chi2,5)))
            pUPP=[6.41,1.81,1.33,4.13,0.31]
            if(hos==1):
                plt.plot(rdebin,UPPlog(rdebin,*pUPP),label='UPP',color='black')
            plt.xlim(0.01,10)
            #plt.ylim(0.0001,100)
            plt.ylim(-6,2)
            plt.xscale('log')
            plt.xlabel("$R/R_{500}$")
            plt.ylabel("$P/P_{500}$")
            if(hos==1):
                plt.title('$log(\dfrac{M_{500}}{M_{\odot}})$='+str(min)+'$\pm0.2$, $n_{elem}$= '+str(n))
            #plt.title('$M_{500}$='+str(min) +'$\pm0.2$E10$M_{\odot}/h$' +',$n_{elem}$= '+str(n))
                #plt.title('All clusters fit')
            plt.legend()
            fitparam.append(popt)
            fitcov.append(pcovdiag)
    #with open('fitparam15to13TNG300Sublog2807.csv','w') as d:
    #    writer=csv.writer(d)
    #    writer.writerows(fitparam)

    #with open('fitparam15to13TNG300Sublogcov2807.csv','w') as da:
    #    writer=csv.writer(da)
    #    writer.writerows(fitcov)
    plt.tight_layout(pad=0.4)
    plt.show()


def fitparamtab(): #Opens the file with the fitted parameters of gNFW, if the lines ax and tab are activated it plots the table of the params, if those lines are off you can do a linear fit with the mass for one parameter and plot it by activating all the plt lines 
    fig=plt.figure()
    ax=fig.add_subplot(111)
    ax.axis("off")
    a=pd.read_csv("fitparam15to13TNG300Sublog1507.csv")
    c=pd.read_csv("fitparam15to13TNG300Sublogcov1507.csv")
    b=[[],[],[],[],[]]
    n=5
    for i in range(0,len(a["p"])):
        b[0].append(str(np.round(a["p"][i],2))+' $\pm$'+str(np.round(c["p"][i],5)))
        b[1].append(str(np.round(a["c"][i],2))+' $\pm$'+str(np.round(c["c"][i],5)))
        b[2].append(str(np.round(a["a"][i],2))+' $\pm$'+str(np.round(c["a"][i],5)))
        b[3].append(str(np.round(a["b"][i],2))+' $\pm$'+str(np.round(c["b"][i],5)))
        b[4].append(str(np.round(a["g"][i],6))+' $\pm$'+str(np.round(c["g"][i],5)))
    #print(b[0][0])
    row=["log($\dfrac{M_{500}}{M_{\odot}/h}$)="+str(np.round(14.6-0.4*i,2)) for i in range(n)]
    col=["$P_0$","$c_{500}$",chr(945),chr(946),"$\gamma$"]
    tab=plt.table(cellText=b,loc="center",cellLoc='center',rowLabels=col,colLabels=row)
    tab.auto_set_font_size(False)
    tab.set_fontsize(10)
    p0=[a["p"][i] for i in range(n)]
    c500=[a["c"][i] for i in range(n)]
    alpha=[a["a"][i] for i in range(n)]
    beta=[a["b"][i] for i in range(n)]
    gamma=[a["g"][i] for i in range(n)]
    stdp0=[c["p"][i] for i in range(n)]
    stdc500=[c["c"][i] for i in range(n)]
    stdalpha=[c["a"][i] for i in range(n)]
    stdbeta=[c["b"][i] for i in range(n)]
    stdgamma=[c["g"][i] for i in range(n)]
    x=np.zeros(n)
    for i in range(n):
        x[i]=14.6-0.4*i
    UPPparam=[6.41,1.81,0.31,1.33,4.13]
    val=UPPparam[0]
    param=p0
    std=stdp0
    UPPval=[val for i in range(5)]
    popt,pcov=curve_fit(pol1,x[0:4],param[0:4],sigma=std[0:4],absolute_sigma=True)
    print("fit val "+str(popt))
    print("cov a param: "+str(pcov[0][0]))
    print("cov b param: "+str(pcov[1][1]))
    #plt.subplot(2,1,1)
    titrefit='fit curve, a='+str(np.round(popt[0],4))+', b='+str(np.round(popt[1],4))
    #plt.plot(x[0:4],pol1(x[0:4],*popt),label=titrefit)
    #plt.plot(x,param,'.')
    #plt.plot(x,UPPval,label="UPP param from Planck V")
    #plt.errorbar(x,param,yerr=std,marker='.',ls='None',label="fit params")
    #plt.xticks(x)
    #plt.ylabel('fit param value')
    #plt.title("Values of the fitted p0 parameter")
    #plt.legend()
    #plt.subplot(2,1,2)
    #relatdif=[(val-param[i])/val for i in range(n)]
    #plt.plot(x,relatdif,'.')
    #plt.ylabel('$\dfrac{Fit value - UPP value}{UPP value}$')
    #plt.xlabel("Mass in M_sun in log scale")
    plt.show()



def clszhideki(mmin,mmax,zmin,zmax,titre,s8,UPP): #Your code with small modifications and tests 
        ### Physics const [cgs]
        sigt = const.sigma_T.cgs.value
        mpc2cm = u.parsec.cgs.in_units('cm') * 1e6
        mec2 = (const.m_e * const.c**2).to(u.keV).value
        Msun = const.M_sun.to(u.g).value
    
        ### Import colossus
        from colossus.cosmology import cosmology
        from colossus.lss import mass_function
        from colossus.halo import concentration
        from colossus.halo import mass_defs
        #if(cp==0):
        cosmo = cosmology.setCosmology('planck15')
        #cosmo.ns=s8
        #cosmo.checkForChangedCosmology()
        #else:    
        #    cosmo = cosmology.setCosmology('WMAP7')
        cosmo_astropy = cosmo.toAstropy()
        H0 = cosmo.H0
        h100 = H0/100.
        h70 = H0/70.
        print("cosmo planck15"+str(cosmo))
        #print("cosmo wmap7"+str(cosmoWMAP7))
        ### Binning
        zbin = 500; mbin = 100; xbin = 501;
    
        #zmin=1e-5; zmax=5; 
        lnx1=np.log(1.+zmin); lnx2=np.log(1.+zmax); 
        dlnx=(lnx2-lnx1)/(zbin-1)
        lnx=np.linspace(lnx1,lnx2, zbin)
        z0=np.exp(lnx)-1
        z=np.tile(z0,mbin).reshape(mbin,zbin).T
    
        dV0 = cosmo_astropy.differential_comoving_volume(z0).value * h100**3 # [Mpc^3/h^3]
        dV = np.tile(dV0,mbin).reshape(mbin,zbin).T
    
        x = np.linspace(1e-5,6,xbin) # xbin 500
        dx = x[1] - x[0]

        ##############################
        ### Main ( calc Cl )
        ##############################
    
        #Mmin = np.log10(5e11); Mmax = np.log10(5e15);
        Mmin=np.log10(mmin);Mmax=np.log10(mmax)
        dlnM = (np.log(5e15)-np.log(5e11))/mbin
        Mass = 10**np.linspace(Mmin, Mmax, mbin) # Mvir Mpc/h
        dn_dVdz = np.zeros((zbin,mbin))
        print("mass "+str(Mass))
    
        ### Calc M500 and R500
        Mvir_h100 = np.tile(Mass,zbin).reshape(zbin,mbin)
        
        cvir = np.zeros((zbin, mbin))
        for I in range(zbin):
            cvir[I] = concentration.concentration(Mvir_h100[I], 'vir', z0[I], model = 'duffy08')

        M500c_h100 = np.zeros((zbin, mbin))
        R500c_h100_kpc = np.zeros((zbin, mbin))
        for I in range(zbin):
            M500c_h100[I], R500c_h100_kpc[I], c500c = mass_defs.changeMassDefinition(Mvir_h100[I], cvir[I], z0[I], 'vir', '500c')
    
        R500c_h100 = R500c_h100_kpc/1e3 # Mpc/h
        M500c_h70 = M500c_h100/h100*h70
    
        ### Calc dn/dVdz
        for I in range(zbin):
            dn_dVdz[I] = mass_function.massFunction(M500c_h100[I], z0[I], mdef = '500c', model = 'tinker08', q_out = 'dndlnM') * dlnM

        ### Calc E(z)
        Ez = cosmo.Ez(z)
        from colossus.lss import bias

        ### Calc halo bias
        hbias = np.zeros((zbin,mbin))
        for I in range(zbin):
            hbias[I] = bias.haloBias(Mass, model = 'tinker10', z = z0[I], mdef = 'vir')

        ### Pressure profile ###
        #P0 = 6.41
        #c500 = 1.81
        #gamma = 0.31
        #alpha = 1.33
        #beta = 4.13
        ##a_p = 0.12
        a_p = 0.

        if(UPP==0):
            P0=[p0(np.log10(Mass[i])) for i in range(len(Mass))]
            c500=[c_500(np.log10(Mass[i])) for i in range(len(Mass))]
            gamma=[Gamma(np.log10(Mass[i])) for i in range(len(Mass))]
            alpha=[Alpha(np.log10(Mass[i])) for i in range(len(Mass))]
            beta=[Beta(np.log10(Mass[i])) for i in range(len(Mass))]
        
        elif(UPP==1):
            P0=[6.41 for i in range(len(Mass))]
            c500=[1.81 for i in range(len(Mass))]
            gamma=[0.31 for i in range(len(Mass))]
            #gamma=[0.096 for i in range(len(Mass))]
            alpha=[1.33 for i in range(len(Mass))]
            beta=[4.13 for i in range(len(Mass))]

        elif(UPP==2):
            P0=[7.7179 for i in range(len(Mass))]
            c500=[1.7899 for i in range(len(Mass))]
            gamma=[0.096 for i in range(len(Mass))]
            #gamma=[0.31 for i in range(len(Mass))]
            alpha=[1.6163 for i in range(len(Mass))]
            beta=[6.5090 for i in range(len(Mass))]

        elif(UPP==3):
            P0=[p02(np.log10(Mass[i])) for i in range(len(Mass))]
            c500=[c_5002(np.log10(Mass[i])) for i in range(len(Mass))]
            gamma=[0.096 for i in range(len(Mass))]
            alpha=[1.6163 for i in range(len(Mass))]
            beta=[Beta2(np.log10(Mass[i])) for i in range(len(Mass))]

        elif(UPP==4):
            P0=[6.41 for i in range(len(Mass))]
            c500=[1.81 for i in range(len(Mass))]
            #gamma=[0.31 for i in range(len(Mass))]
            gamma=[0.096 for i in range(len(Mass))]
            alpha=[1.33 for i in range(len(Mass))]
            beta=[4.13 for i in range(len(Mass))]

        elif(UPP==5):
            a=1.11453205e+01
            alpha=1.25849346e+00
            beta=7.86529735e+00
            b=4.07687058e-04

            
           
        
        print("m500c_h70"+str(M500c_h70))
        p_x=np.zeros((mbin,xbin))
        for i in range(mbin):
            if(UPP<5):
                p_x[i]=P0[i] / ( ((c500[i]*x)**(gamma[i])) * ((1 + ((c500[i]*x)**alpha[i]))**((beta[i]-gamma[i])/alpha[i])) )
            else:
                p_x[i]=a/(1+x**alpha)**beta+b
        print("p_x"+str(p_x))
        p500 = 1.65e-3 * (Ez**(8/3.)) * (M500c_h70/(3e14))**(2/3.) * (h70**2)
        p_r = p500[:,None:,None] * (M500c_h70[:,None:,None]/(3e14))**(a_p) * p_x[None,None:,:]
        print("p_r"+str(p_r))

        
        ### Calc Cyy ###
        rs = R500c_h100 # [Mpc/h] x=r/rs, rs=rvir/c
        DA_Mpc_h100 = cosmo.angularDiameterDistance(z) # [Mpc/h]
        ls = DA_Mpc_h100/rs

        X_Mpc0_h100 = -cosmo.comovingDistance(z0) # [Mpc/h]

        print("")
        print("Calculating Cl....")
        start = time.time()
        M = 0
        ell = np.logspace(0,4,50)
        cl1 = np.zeros(len(ell)); cl2 = np.zeros(len(ell));

        def calcul(M):
            for l in ell:
    
                if (l == 0):
                    continue; 

                print("l =", l)
                end = time.time()
                print("process time = %f (min)" %( (end - start)/60 )  )
                
                theta = l*x/ls[:,None:,None]
                yl = (4*np.pi*rs/h100/ls**2)*(sigt/mec2) * np.nansum(dx * x**2 * np.sin(theta)/(theta) * p_r * mpc2cm, axis=2)
                
                cl1[M] = np.nansum((dlnx * (1+z) * dV * (dn_dVdz)) * yl**2)
                
                Pk_z = np.zeros(zbin)
                for N in range(zbin):
                    Pk_z[N] = cosmo.matterPowerSpectrum(l/X_Mpc0_h100[N], z=z0[N])
                
                cl2[M] = np.nansum(dlnx * (1+z0) * dV0 *  np.nansum(dn_dVdz * hbias * yl, axis=1)**2 * Pk_z)
                print(l*(l+1)*cl1[M]/(2*np.pi), l*(l+1)*cl2[M]/(2*np.pi))
        
                M = M + 1
        calcul(M)
        end = time.time()
        print("end time = %f (min)" %( (end - start)/60 )  )


        # beam
        FWHM=10.
        Sigma = FWHM/2.354820
        Sigma_rad = Sigma/60. * (np.pi/180)
        bl = np.exp(-ell*(ell+1)*(Sigma_rad**2)/2.)
        
        cl = cl1 + cl2
        
        cl1_smth = cl1*bl
        cl2_smth = cl2*bl
        cl_smth = cl*bl
        
        conv = ell*(ell+1)/(2*np.pi)
    
        dl1 = conv*cl1
        dl2 = conv*cl2
        dl = conv*cl
        dl1_smth = conv*cl1_smth
        dl2_smth = conv*cl2_smth
        dl_smth = conv*cl_smth
        
        #plt.figure()
        #plt.loglog(ell, dl1, 'b', label='1h')
        #plt.loglog(ell, dl1_smth, 'b--')
        #plt.loglog(ell, dl2, 'g', label='2h')
        #plt.loglog(ell, dl2_smth, 'g--')
        plt.loglog(ell, dl, label=titre)
                  # +", $n_s=$"+str(np.round(s8,5)))
        #plt.loglog(ell, dl_smth, 'r--')
        plt.xlim(2,3e3)
        plt.ylim(1e-15,1e-11)
        plt.xlabel('Multipole $l$'); 
        plt.ylabel('$l(l+1)C_l/2\\pi$'); 
        plt.legend(loc='upper left')
        plt.grid(True)
        #plt.show()



###Maths functions ###

def pol2(x,a,b,c):
    return a*x**2+b*x+c

def pol1(x,a,b):
    return a*x+b

def UPPlog(x,P0,c500,alpha,beta,gamma):
    return np.log10(P0)-gamma*np.log10(c500*x)+((gamma-beta)/alpha)*np.log10(1+(c500*x)**alpha)

#functions of gNFW params depending on the mass based on the linear fit, 5 first from the first fit and 3 others from the second one
def p0(M):
    return 4.2739*M-50.9111
        
def c_500(M):
    return 2.3124*M-29.6599

def Alpha(M):
    return 0.6599*M-7.4953

def Beta(M):
    return -2.9388*M+47.4093

def Gamma(M):
    return 0.0666*M-0.8762

def c_5002(M):
    return 0.4074*M-3.6778

def p02(M):
    return 2.2669*M-22.2573

def Beta2(M):
    return 1.5273*M-14.1321

def einasto(x,a,alpha,b,c):
    return a*(2/alpha)*(1-(c*x)**alpha)+b

def betamodel(x,a,alpha,beta,b):
    return np.log10(a/(1+x**alpha)**beta+b)

def pressint(x,P0,c500,alpha,beta,gamma): #Tries to integrate the pressure profile
    return  (P0*(x)**2)/(((c500*x)**gamma)*((1+(c500*x)**alpha)**((beta-gamma)/alpha)))

def UPPfit(x,P0,c500,alpha,beta,gamma): #function used to fit with UPP
    return  (P0)/(((c500*x)**gamma)*((1+(c500*x)**alpha)**((beta-gamma)/alpha)))


### Plotting functions ###


        
def UPP(P0,c500,alpha,beta,gamma,stop,title,col): #plot Pressure profile from Universal Pressure Profile model (GNFW model) in fonctions of the 5 params from the model, stop is the end of plot in R/R_500, used in compar and profilexmaps functions 
    profile=[]
    for i in range(1,int(stop*1000)+1):
        profile.append(P0/(((c500*(i/1000))**gamma)*((1+(c500*(i/1000))**alpha)**((beta-gamma)/alpha))))
    x=np.arange(0,stop,0.001)
    plt.plot(x,profile,color=col,label=title)
    plt.xlim(0.01,stop)

def UPPx2(P0,c500,alpha,beta,gamma,stop,title,col): #plot Pressure profile multiplied by x² from Universal Pressure Profile model (GNFW model) in fonctions of the 5 params from the model, stop is the end of plot in R/R_500 
    profile=[]
    for i in range(1,int(stop*1000)+1):
        profile.append((P0*(i/1000)**2)/(((c500*(i/1000))**gamma)*((1+(c500*(i/1000))**alpha)**((beta-gamma)/alpha))))
    x=np.arange(0,stop,0.001)
    plt.plot(x,profile,color=col,label=title)
    plt.xlim(0.01,stop)
    
def map(d,h,nb,dir1,dir2):#2D map of the halo, with R_500 and 2R_500 circles, d is the dataset, h the halo infos, dir1 and dir2 the axis choosen 
     R500=h['Group_R_Crit500']
     plt.hist2d(d['Coordinates'][:,dir1],d['Coordinates'][:,dir2],norm=mpl.colors.LogNorm(),bins=500) #plotting the map 
     #creating the 2 circles and the central point
     theta=np.linspace(0,2*np.pi,100)
     x1=h['GroupCM'][dir1]+R500*np.cos(theta)
     x2=h['GroupCM'][dir2]+R500*np.sin(theta)
     x3=h['GroupCM'][dir1]+2*R500*np.cos(theta)
     x4=h['GroupCM'][dir2]+2*R500*np.sin(theta)
     plt.plot(x1,x2,color='red',label='$R_{500}$')
     plt.plot(x3,x4,color='black',label='$2R_{500}$')
     plt.plot(h['GroupCM'][dir1],h['GroupCM'][dir2],'.',markersize=10,color='red')
     len_x=max(d['Coordinates'][:,dir1])- min(d['Coordinates'][:,dir1])
     len_y=max(d['Coordinates'][:,dir2])- min(d['Coordinates'][:,dir2])
     #naming the axis
     if(dir1==0):
         xname='x'
     elif(dir1==1):
         xname='y'
     else:
         xname='z'
     if(dir2==0):
         yname='x'
     elif(dir2==1):
         yname='y'
     else:
         yname='z'
     plt.xlabel(xname)
     plt.ylabel(yname)
     #plt.title('halo ID:'+str(nb))
     m=max(len_x,len_y)
     #putting boundaries in order to avoid strange plots and to have the center of mass in the center of the plot
     plt.xlim(h['GroupCM'][dir1]-0.5*m,h['GroupCM'][dir1]+0.5*m)
     plt.ylim(h['GroupCM'][dir2]-0.5*m,h['GroupCM'][dir2]+0.5*m)
     plt.colorbar()
     plt.legend(loc='upper right')
     #plt.show()

def mapsub(d,h,s,nb,dir1,dir2,r500):#2D map of the halo but this time for the subfind catalog, with R_500 and 2R_500 circles, d is the dataset, h the group, dir1 and dir2 the axis choosen, same comments as the previous function 
     plt.hist2d(d['Coordinates'][:,dir1],d['Coordinates'][:,dir2],norm=mpl.colors.LogNorm(),bins=500)
     theta=np.linspace(0,2*np.pi,100)
     x1=s['SubhaloCM'][dir1]+r500*np.cos(theta)
     x2=s['SubhaloCM'][dir2]+r500*np.sin(theta)
     x3=s['SubhaloCM'][dir1]+2*r500*np.cos(theta)
     x4=s['SubhaloCM'][dir2]+2*r500*np.sin(theta)
     plt.plot(x1,x2,color='red',label='$R_{500}$')
     plt.plot(x3,x4,color='black',label='$2R_{500}$')
     plt.plot(s['SubhaloCM'][dir1],s['SubhaloCM'][dir2],'.',markersize=10,color='red',label='CM')
     len_x=max(d['Coordinates'][:,dir1])- min(d['Coordinates'][:,dir1])
     len_y=max(d['Coordinates'][:,dir2])- min(d['Coordinates'][:,dir2])
     if(dir1==0):
         xname='x'
     elif(dir1==1):
         xname='y'
     else:
         xname='z'
     if(dir2==0):
         yname='x'
     elif(dir2==1):
         yname='y'
     else:
         yname='z'
     plt.xlabel(xname)
     plt.ylabel(yname)
     m=max(len_x,len_y)
     plt.xlim(h['GroupCM'][dir1]-0.5*m,h['GroupCM'][dir1]+0.5*m)
     plt.ylim(h['GroupCM'][dir2]-0.5*m,h['GroupCM'][dir2]+0.5*m)
     plt.colorbar()
     plt.legend(loc='upper right')


def pressure_profile(d,m500,haloR500,halocenter,i):#plots the pressure profile in log scales
    mi,ma,n,t,P,nb=mean_r(d,halocenter)
    r=range(int(ma)+1)
    r/=haloR500
    P_500=np.sum(P[0:int(haloR500)+1]*nb[0:int(haloR500)+1])/np.sum(nb[0:int(haloR500)+1])
    P/=P_500
    name='ID: '+str(i)+' , $M_{500}$='+format(m500*1E10,'.3E')+' $M_\odot/h$'
    plt.plot(r,P,'.',label=name,markersize=4)
    plt.xlim(0.01,4)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel("$R/R_{500}$")
    plt.ylabel("$P/P_{500}$")
    

def compar(deb,nb): #plot multiple pressure profiles of id from deb to deb+nb from the FoF catalog and the  UPP profile
    for i in range(deb,nb+deb):
        d,g,halocenter,haloR500,m500=opend('TNG100_3',99,i,0)
        firstsub=g['GroupFirstSub']
        
        pressure_profile(d,m500,haloR500,halocenter,i)
    UPP(6.41,1.81,1.33,4.13,0.31,4)
    plt.legend()
    plt.show()


def profilexmaps(id): #pressure profile + 3 projected maps of a cluster (friends-of-friends)
    d,g,halocenter,haloR500,m500=opend('TNG300_1',99,id,0)
    plt.figure(figsize=(12,12))
    plt.subplot(2,2,1)
    pressure_profile(d,m500,haloR500,halocenter,id)
    plt.title("Pressure Profile")
    UPP(6.41,1.81,1.33,4.13,0.31,4,'UPP, best fit params from Planck V','black')
    plt.legend()
    plt.subplot(2,2,2)
    map(d,g,id,0,1)
    plt.subplot(2,2,3)
    map(d,g,id,0,2)
    plt.subplot(2,2,4)
    map(d,g,id,1,2)
    plt.show()

    

def sub(h): #plot multiple pressure profiles of id from deb to deb+nb (from subfind)
    sim='TNG100_3'
    p='/data/cluster/byopic/SIMS/'+sim
    g=il.groupcat.loadSingle(p,99,haloID=h)
    firstsub=g['GroupFirstSub']
    s=il.groupcat.loadSingle(p,99,subhaloID=firstsub)
    halocenter=s['SubhaloCM']
    haloR500=g['Group_R_Crit500']
    d=il.snapshot.loadSubhalo(p,99,firstsub,'gas')
    data=pd.read_csv('r500-m500,0-10.csv')
    r500=data["r500"][h]
    m500=data["m500"][h]
        
    mi,ma,n,t,P,nb=mean_r(d,halocenter)
    r=range(int(ma)+1)
    r/=r500
    P_500=np.sum(P[0:int(r500)+1]*nb[0:int(r500)+1])/np.sum(nb[0:int(r500)+1])
    P/=P_500
    name='hID:'+str(h)+' subhID:'+str(firstsub)+ ', $M_{500}$='+format(m500*1E10,'.3E')+' $M_\odot/h$'

    plt.figure(figsize=(12,12))
    plt.subplot(2,2,1)
    plt.plot(r,P,'.',label=name,markersize=4)
    plt.xlim(0.01,4)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel("$R/R_{500}$")
    plt.ylabel("$P/P_{500}$")
    plt.title("pressure_profile of the biggest subhalo")
    UPP(6.41,1.81,1.33,4.13,0.31,4,'UPP, best fit params from Planck V','black')
    UPP(8.827,1.177,1.0510,5.4905,0.3081,4,'UPP params from Arnaud et al. 2010','red')
    plt.legend()
    plt.subplot(2,2,2)
    mapsub(d,g,s,id,0,1,r500)
    plt.subplot(2,2,3)
    mapsub(d,g,s,id,0,2,r500)
    plt.subplot(2,2,4)
    mapsub(d,g,s,id,1,2,r500)

    plt.show()


def testnewUPP(mmin,mmax,mbin): #tests of the shape of the modified upp , activate the wanted lines to have various profiles 
    Mmin=np.log10(mmin);Mmax=np.log10(mmax)
    Mass = 10**np.linspace(Mmin, Mmax, mbin)
    print("Mass:"+str(Mass))
    P0=[p02(np.log10(Mass[i])) for i in range(len(Mass))]
    c500=[c_5002(np.log10(Mass[i])) for i in range(len(Mass))]
    #gamma=[Gamma(np.log10(Mass[i])) for i in range(len(Mass))]
    #alpha=[Alpha(np.log10(Mass[i])) for i in range(len(Mass))]
    beta=[Beta2(np.log10(Mass[i])) for i in range(len(Mass))]
    #P0=[6.41 for i in range(len(Mass))]
    #c500=[1.81 for i in range(len(Mass))]
    #gamma=[0.31 for i in range(len(Mass))]
    #alpha=[1.33 for i in range(len(Mass))]
    #beta=[4.13 for i in range(len(Mass))]
    gamma=[0.096 for i in range(len(Mass))]
    alpha=[1.6163 for i in range(len(Mass))]
    print("P0:"+str(P0))
    print("c500:"+str(c500))
    print("gamma:"+str(gamma))
    print("alpha:"+str(alpha))
    print("beta:"+str(beta))
    x = np.linspace(1e-5,6,501)
    p_x=np.zeros((mbin,501))
    for i in range(0,mbin):
        #p_x[i]=P0[i] / ( ((c500[i]*x)**(gamma[i])) * ((1 + ((c500[i]*x)**alpha[i]))**((beta[i]-gamma[i])/alpha[i])) )
        p_x[i]=np.log10(P0[i])-gamma[i]*np.log10(c500[i]*x)+((gamma[i]-beta[i])/alpha[i])*np.log10(1+(c500[i]*x)**alpha[i])
    for i in range(0,mbin):
        plt.plot(x,p_x[i],label="M="+format(Mass[i],'.1E')+"$M_{\odot}$")
    P0 = 6.41
    c500 = 1.81
    gamma = 0.31
    alpha = 1.33
    beta = 4.13
    p_UPP=np.zeros(501)
    #p_UPP=P0 / ( ((c500*x)**(gamma)) * ((1 + ((c500*x)**alpha))**((beta-gamma)/alpha)) )
    p_UPP=np.log10(P0)-gamma*np.log10(c500*x)+((gamma-beta)/alpha)*np.log10(1+(c500*x)**alpha)
    plt.plot(x,p_UPP,label='UPP',color='black')
    plt.title("Pressure profiles, mass-dependant params")
    plt.xlabel("$R/R_{500}$")
    plt.ylabel("$P/P_{500}$")
    plt.xscale('log')
    plt.xlim(0.01,10)
    plt.legend()
    plt.show()
    #print(chr(946))
     
def fitparamtab_betamodel(): #same as fitparamtab above but for the beta model fit 
    fig=plt.figure()
    #ax=fig.add_subplot(111)
    #ax.axis("off")
    a=pd.read_csv("fitparam15to13TNG300Sublog2107beta70.csv")
    c=pd.read_csv("fitparam15to13TNG300Sublogcov2107beta70.csv")
    b=[[],[],[],[]]
    n=5
    for i in range(0,len(a["a"])):
        b[0].append(str(np.round(a["a"][i],2))+' $\pm$'+str(np.round(c["a"][i],5)))
        b[1].append(str(np.round(a["alpha"][i],2))+' $\pm$'+str(np.round(c["alpha"][i],5)))
        b[2].append(str(np.round(a["beta"][i],2))+' $\pm$'+str(np.round(c["beta"][i],5)))
        b[3].append(str(np.round(a["b"][i],5))+' $\pm$'+str(np.round(c["b"][i],7)))
        #b[4].append(str(np.round(a["g"][i],2))+' $\pm$'+str(np.round(c["g"][i],5)))
    #print(b[0][0])
    row=["10^"+str(np.round(14.6-0.4*i,2))+" $M_{\odot}$" for i in range(n)]
    col=["a","alpha","beta","b"]
    #tab=plt.table(cellText=b,loc="center",cellLoc='center',rowLabels=col,colLabels=row)
    #tab.auto_set_font_size(False)
    #tab.set_fontsize(10)
    ap=[a["a"][i] for i in range(n)]
    #=[a["alpha"][i] for i in range(n)]
    alpha=[a["alpha"][i] for i in range(n)]
    beta=[a["beta"][i] for i in range(n)]
    b=[a["b"][i] for i in range(n)]
    stda=[c["a"][i] for i in range(n)]
    #stdc500=[c["c"][i] for i in range(n)]
    stdalpha=[c["alpha"][i] for i in range(n)]
    stdbeta=[c["beta"][i] for i in range(n)]
    stdb=[c["b"][i] for i in range(n)]
    x=np.zeros(n)
    for i in range(n):
        x[i]=14.6-0.4*i
    #UPPparam=[6.41,1.81,0.31,1.33,4.13]
    UPPbetafitparam=[56,67,0.6407,8.47,1.03e-6]
    #val=UPPparam[0]
    param=b
    std=stdb
    #UPPval=[val for i in range(5)]
    popt,pcov=curve_fit(pol1,x[0:4],param[0:4],sigma=std[0:4],absolute_sigma=True)
    print("fit val "+str(popt))
    print("cov a param: "+str(pcov[0][0]))
    print("cov b param: "+str(pcov[1][1]))
    #plt.subplot(2,1,1)
    titrefit='fit curve, a='+str(np.round(popt[0],4))+', b='+str(np.round(popt[1],4))
    plt.plot(x[0:4],pol1(x[0:4],*popt),label=titrefit)
    plt.plot(x,param,'.')
    #plt.plot(x,UPPval,label="UPP param from Planck V")
    plt.errorbar(x,param,yerr=std,marker='.',ls='None',label="fit params")
    plt.xticks(x)
    plt.ylabel('fit param value')
    plt.title("Values of the fitted b parameter")
    plt.legend()
    #plt.subplot(2,1,2)
    #relatdif=[(val-param[i])/val for i in range(n)]
    #plt.plot(x,relatdif,'.')
    #plt.ylabel('$\dfrac{Fit value - UPP value}{UPP value}$')
    plt.xlabel("log($\dfrac{M_{500}}{M_{\odot}}$)")
    plt.show()


def overplot(sim,snap,start,end): #overplot of the fof and sub pressure profiles using massbin0 for TNG100_3
    r0,P0,std0,m0,n0=massbinO(sim,snap,start,end,0)
    r1,P1,std1,m1,n1=massbinO(sim,snap,start,end,1)
    plt.figure(figsize=(12,12))
    deb=0
    fin=70
    for i in range(0,len(r0)):
        plt.subplot(3,2,i+1)
        plt.scatter(r0[i][deb:fin],P0[i][deb:fin],s=4)
        plt.errorbar(r0[i][deb:fin],P0[i][deb:fin],yerr=std0[i][deb:fin],marker='.',ls='None',label='FoF')
        plt.scatter(r1[i][deb:fin],P1[i][deb:fin],s=4)
        plt.errorbar(r1[i][deb:fin],P1[i][deb:fin],yerr=std1[i][deb:fin],marker='.',ls='None',label='Subfind')
        pUPP=[6.41,1.81,1.33,4.13,0.31]
        #rdebin=[10**(i*0.05) for i in range(80)]
        #rdebin/=
        plt.plot(r0[i],UPPlog(r0[i],*pUPP),label='UPP',color='black')
        plt.xlim(0.01,10)
        plt.ylim(-6,2)
        plt.xscale('log')
        #plt.yscale('log')
        plt.xlabel("$R/R_{500}$")
        plt.ylabel("$P/P_{500}$")
        plt.title('log($\dfrac{M_{500}}{M_{\odot}/h}$)='+str(m0[i])+'$\pm0.1$, '+',$n_{Fof}$= '+str(n0[i])+',$n_{Sub}$='+str(n1[i]))
        #UPP(6.41,1.81,1.33,4.13,0.31,4,'UPP','black')
        plt.legend(loc="lower left")
    plt.tight_layout(pad=0.4)
    plt.show()

def masscomparFofvsSub(sim,snap,deb,fin): #comparison of the masses of a same halo from fof and subfind catalog 
    p='/data/cluster/byopic/SIMS/'+sim
    halomass=np.zeros(fin-deb)
    subhalomass=np.zeros(fin-deb)
    for i in range(deb,fin):
        g=il.groupcat.loadSingle(p,snap,haloID=i)
        halomass[i-deb]=g['GroupMass']*1E10
        firstsub=g['GroupFirstSub']
        d=il.groupcat.loadSingle(p,snap,subhaloID=firstsub)
        subhalomass[i-deb]=d['SubhaloMass']*1E10
    plt.subplot(2,1,1)
    plt.plot(halomass,'.',label='halo mass (Fof)')
    plt.plot(subhalomass,'.',label='first sub-halo mass (Subfind)')
    plt.ylabel("Mass (M_sun)")
    plt.yscale('log')
    plt.legend()
    plt.subplot(2,1,2)
    plt.plot(1-subhalomass/halomass,'.',label="(halomass-subhalomass)/halomass")
    plt.legend()
    plt.show()

def press_integ(): #tries to integrate the pressure profile to study the mass bias 
    param=pd.read_csv("fitparam14,1to12,9.csv")
    print(param["g"][0])
    #print(param["p"][0])
    #I=[]
    m=[14.1-0.2*i for i in range(0,8)]
    
    for k in range(0,5):
        I1tot=[]
        I2tot=[]
        I1man=[]
        I2man=[]
        I1manual=0
        I2manual=0
        I=[]
        print(k)
        for j in range(1,20):
            I1,dev1=quad(pressint,0.1,0.2*j,args=(param["p"][k],param["c"][k],param["a"][k],param["b"][k],param["g"][k]))
            #print("Integral from 0 to 4: "+str(I1)+", m_500="+str(round(14.1-0.2*i,2))+"+-0.1 1E10M_sun")
            I2,dev2=quad(pressint,0.1,0.2*j,args=(6.41,1.81,1.33,4.13,0.31))
            I.append((I1-I2)/I2)
            I1manual+=0.2*pressint(j*0.2,param["p"][k],param["c"][k],param["a"][k],param["b"][k],param["g"][k])
            I2manual+=0.2*pressint(j*0.2,6.41,1.81,1.33,4.13,0.31)
            print(j)
            print("I1quad= "+str(I1))
            print("I2quad= "+str(I2))
            print("I2manual= "+str(I1manual))
            print("I1manual= "+str(I2manual))
            I1tot.append(I1)
            I2tot.append(I2)
            I1man.append(I1manual)
            I2man.append(I2manual)
            
        plt.subplot(3,3,k+1)
        r=[0.2*i for i in range(1,20)]
        a=[pressint(0.1*i,6.41,1.81,1.33,4.13,0.31) for i in range(1,40)]
        b=[pressint(0.1*i,param["p"][k],param["c"][k],param["a"][k],param["b"][k],param["g"][k]) for i in range(1,40)]
        plt.plot(r,I1tot,label="$I_{fit (quad)}$")
        plt.plot(r,I2tot,label="$I_{UPP (quad)}$")
        plt.plot(r,I1man,label="$I_{fit (manual)}$")
        plt.plot(r,I2man,label="$I_{UPP (manual)}$")
        plt.xlabel("R/$R_{500}$")
        plt.ylabel("Integrated $r^{2}*P(r)$ in [0,R]")
        plt.title("mass bin:"+str(round(m[k],2))+'E10 $M_\odot$')
        plt.legend()
    plt.tight_layout(pad=0.4)
    plt.show()