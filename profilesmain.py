#libs
import illustris_python as il
import matplotlib.pyplot as plt
import matplotlib as mpl
import math
import numpy as np
import functions as f
from astropy.cosmology import Planck15 as cosmo
from astropy.constants import M_sun,kpc
import csv
import pandas as pd
from scipy.optimize import curve_fit


#####main (only functions call, go to functions.py to see them)####
#There are only little functions that compile the one from functions.py, or i do little test 

#f.compar(0,5)
#f.profilexmaps(25)
#f.sub(25)
#f.massbin('TNG100_3',99,0,150,1)
#f.r500m500computing()
#f.overplot('TNG100_3',99,0,150)
#f.masscomparFofvsSub('TNG100_3',99,0,50)
#f.press_integ()
#f.profilexmaps(2000)
#f.press_storage('TNG300_1',99,0,1000,1)

#f.fitparamtab()
#f.massbinTNG300(0,150,0)
f.massbinTNG300(0,1000,1)
#plt.legend()
#plt.show()
val=0.9667
p=1.1*val
m=0.9*val
def tsz():
    plt.figure()
    #f.clszhideki(1e13,5e15,1e-5,3,"UPP, Planck15",p,1)
    #f.clszhideki(1e13,5e15,1e-5,3,"UPP, Planck15",val,1)
    #f.clszhideki(1e13,5e15,1e-5,3,"UPP, Planck15",m,1)
    f.clszhideki(3e13,5e14,1e-5,3," Planck15, UPP",1,1)
    f.clszhideki(3e13,5e14,1e-5,3,"Planck15, 5 mass depending parameters",1,0)
    f.clszhideki(3e13,5e14,1e-5,3,"Planck15, 3 mass depending params, 2 fixed",1,3)
    f.clszhideki(3e13,5e14,1e-5,3,"Planck15, total mass fit, 5 new fixed parameters",1,2)
    
    plt.title("tSZ power spectra, Planck15 cosmo parameters")
    plt.legend()
    plt.show()
#tsz()
#f.testnewUPP(1e13,5e15,10)

def testnewmodel():
    x=np.arange(0.01,50,0.05)
    #a=f.einasto(x,1,1,0,1)
    #c=f.einasto(x,0.5,1,0)
    #d=f.einasto(x,1.5,1,0)
    b=f.UPPlog(x,6.41,1.81,1.33,4.13,0.33)
    d=f.betamodel(x,56.47,0.6407,8.47,1.03809e-6)
    e=f.betamodel(x,56.47,0.6407,8.47,2e-6)
    g=f.betamodel(x,56.47,0.6407,8.47,0.1e-6)
    #plt.plot(x,a,label="a=1,alpha=1")
    #plt.plot(x,c,label="a=0.5,alpha=1")
    plt.plot(x,d,label="beta model,a=56.47,alpha=0.6407,beta=8.47,b=1.03809e-6")
    plt.plot(x,e,label="beta model,b=2e-6")
    plt.plot(x,g,label="beta model,b=1e-7")
    #plt.plot(x,b,label="UPP")
    popt,pcov=curve_fit(f.betamodel,x,b)
    print("popt="+str(popt))
    #plt.plot(x,f.betamodel(x,*popt),label="beta model fit")
    plt.xscale('log')
    plt.xlim(1e-2,50)
    plt.ylim(-6,2)
    plt.xlabel("$r/r_{500}$")
    plt.ylabel("$P/P_{500}$")
    plt.legend()
    plt.show()
#testnewmodel()
#f.fitparamtab_betamodel()

def overplot1halo(hid):
    d,g,halocenter,haloR500,m500=f.opend('TNG100_3',99,hid,0)
    mi,ma,n,t,P,nb=f.mean_r(d,halocenter)
    P_500=np.nansum(P[0:int(haloR500)+1]*nb[0:int(haloR500)+1])/np.nansum(nb[0:int(haloR500)+1])
    P/=P_500
    d,g,halocenter,haloR5001,m500=f.opend('TNG100_3',99,hid,1)
    mi,ma,n,t,P1,nb1=f.mean_r(d,halocenter)
    P_5001=np.nansum(P1[0:int(haloR500)+1]*nb1[0:int(haloR500)+1])/np.nansum(nb1[0:int(haloR500)+1])
    P1/=P_5001
    x=[i/haloR500 for i in range(6000)]
    x1=[i/haloR5001 for i in range(6000)]
    plt.subplot(2,3,hid+1)
    plt.plot(x,P,'.',markersize=2,label="Fof")
    plt.plot(x1,P1,'.',markersize=2,label="Subfind")
    #plt.plot(x,f.UPPfit(x,6.41,1.81,1.33,4.13,0.31))
    f.UPP(6.41,1.81,1.33,4.13,0.31,4,'UPP','black')
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel("$R/R_{500}$")
    plt.ylabel("$P/P_{500}$")
    plt.title("Pressure profile of the halo with ID"+str(hid)+", FoF VS Subfind")
    plt.legend()
    #plt.show()

#plt.figure(figsize=(12,12))
#for i in range(0,6):
#    overplot1halo(i)
#plt.tight_layout(pad=0.1)
#plt.show()
